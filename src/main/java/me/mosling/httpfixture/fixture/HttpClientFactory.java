package me.mosling.httpfixture.fixture;

import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;

public class HttpClientFactory
{
    private HttpClientFactory()
    {
        // hide constructor
    }

    public static HttpClient newClientWithConfig( String httpConfig, String jksConfig, boolean lookIntoFileSystem )
    {
        return newClientWithArgs( new HttpClientArgs().configFile( httpConfig ),
                new JksManagerArgs()
                        .configFile( jksConfig )
                        .allowFilesystemLookup( lookIntoFileSystem ) );
    }

    public static HttpClient newClientWithArgs( HttpClientArgs args, JksManagerArgs keyArgs )
    {
        return HttpClient.build( args, null == keyArgs ? null : new JksManager( keyArgs ) );
    }

    public static HttpClient newClientWithJksManager( HttpClientArgs args, JksManager j )
    {
        return HttpClient.build( args, j );
    }

    public static HttpRequest defaultRequest( String method, boolean jsonFlag )
    {
        HttpRequest r = new HttpRequest();
        r.clear();
        r.setMethod( method );

        if ( jsonFlag && !"GET".equalsIgnoreCase( method ) )
        {
            r.addHeader( "Content-Type", "application/json" );
        }

        return r;
    }
}
