package me.mosling.httpfixture.fixture;

import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static me.mosling.httpfixture.fixture.HttpClientFactory.newClientWithArgs;

/**
 * This is a base for a number of test fixtures classes. A test fixture is the glue for integration tests
 * between the test description and the running test system.
 * This base class wraps
 * the HttpClient creation where method httpArgs() and jksManager() can be overwritten to add your own dependencies
 * login user, client or client with user
 * execute an http request
 */
public class BaseHttpFixture
{
    private static final Logger     LOGGER = LogManager.getLogger( BaseHttpFixture.class );
    private              HttpClient client = null;

    public void build( HttpClientArgs httpArgs, JksManagerArgs jksArgs )
    {
        client = newClientWithArgs( httpArgs, jksArgs );
    }

    public void build( HttpClientArgs httpArgs, JksManager jksManager )
    {
        client = HttpClientFactory.newClientWithJksManager( httpArgs, jksManager );
    }

    public HttpClient getClient()
    {
        if ( null == client )
        {
            LOGGER.warn( "client isn't initialized, use client from HttpClientFactory.newDefaultClient" );
            client = newClientWithArgs( new HttpClientArgs(), new JksManagerArgs() );
        }

        return client;
    }

    @SuppressWarnings( { "EmptyMethod", "unused" } )
    public void updateRequest( HttpRequest r )
    {
        // do nothing, placeholder for override
    }

    /**
     * @param method         Http method GET,OPTION,DELETE without content and POST,PATCH,PUT with data or something
     *                       else for testing
     * @param testHostId     test host identifier
     * @param testAuthId     test authorization identifier
     * @param address        the address added to the host url
     * @param content        the content which is send to the host, used with null request only
     * @param expectedStatus the expected http status, or -1 to ignore the test
     * @return response data object
     */
    public ResponseData executeHttp( String method, String testHostId, String testAuthId, String address,
            String content, int expectedStatus )
    {
        List<String> postMethods = Arrays.asList( "POST", "PUT", "PATCH" );

        if ( client == null )
        {
            throw new AssertionError( "HttpClient not initialized, should be not null" );
        }

        boolean     isPostLike  = postMethods.contains( method.toUpperCase( Locale.ENGLISH ) );
        HttpRequest httpRequest = HttpClientFactory.defaultRequest( method, isPostLike );
        updateRequest( httpRequest );
        // add content
        if ( isPostLike && content != null && !content.isEmpty() )
        {
            httpRequest.setTextBody( content );
        }

        return executeRequest( httpRequest, testHostId, testAuthId, address, expectedStatus );
    }

    /**
     * @param request        A HttpRequest object which contains method, post-data and additional headers.
     * @param testHostId     test host identifier
     * @param testAuthId     test authorization identifier
     * @param address        the address added to the host url
     * @param expectedStatus the expected http status, or -1 to ignore the test
     * @return response data object
     */
    public ResponseData executeRequest( HttpRequest request, String testHostId, String testAuthId, String address,
            int expectedStatus )
    {

        if ( client == null )
        {
            throw new AssertionError( "HttpClient not initialized, should be not null" );
        }

        if ( request == null )
        {
            throw new AssertionError( "HttpRequest not initialized, should be not null" );
        }

        // execute request
        ResponseData rd = client.execute( testHostId, testAuthId, address, request );
        rd.showResponse( true, Level.DEBUG );

        if ( expectedStatus > 0 && rd.getStatus() != expectedStatus )
        {
            throw new AssertionError( "expected status was " + expectedStatus + " but is " + rd.getStatus() );
        }

        return rd;
    }
}
