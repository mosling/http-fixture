package me.mosling.httpfixture.security;

import lombok.Getter;
import lombok.Setter;

public class OAuthToken
{
    @Getter @Setter private String accessToken   = "";
    @Getter @Setter private String refreshToken  = "";
    @Getter @Setter private String tokenType     = "";
    @Getter @Setter private String scope         = "";
    @Getter @Setter private Long   expiryTimeMs  = -1L;
    @Getter @Setter private String authEntryName = "";

    @Getter @Setter private String usedClientKey = "";
    @Getter @Setter private String usedHostKey   = "";

    public boolean isValid()
    {
        return !accessToken.isEmpty();
    }

    public Long expiresInSeconds()
    {
        if ( expiryTimeMs > 0L )
        {
            return ( expiryTimeMs - System.currentTimeMillis() ) / 1000L;
        }

        return 0L;
    }
}
