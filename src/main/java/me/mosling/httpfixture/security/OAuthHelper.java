package me.mosling.httpfixture.security;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.EnumAuthType;
import me.mosling.httpfixture.fixture.HttpAuth;
import me.mosling.httpfixture.fixture.HttpClient;
import me.mosling.httpfixture.fixture.HttpClientFactory;
import me.mosling.httpfixture.fixture.HttpRequest;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OAuthHelper
{

    private static final Logger LOGGER                            = LogManager.getLogger( OAuthHelper.class );
    public static final  String CONTENT_TYPE                      = "Content-Type";
    public static final  String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

    private OAuthHelper()
    {
        // helper class
    }

    /**
     * This method get an oauth token token from the authorization server using the api client credentials. And add the
     * current token to the authorization list
     *
     * @param client        using this http client connection
     * @param amServer      the server identifier, must exists in the host map
     * @param clientAuth    the api client identifier, must exists in the auth map
     * @param authEntryName add the token as Bearer authorization with this entry name to the auth map
     * @return true if the token correctly received
     */
    public static OAuthToken getTokenForClient( HttpClient client, String amServer, String clientAuth,
            String authEntryName )
    {
        HttpRequest request = HttpClientFactory.defaultRequest( "POST", false );

        request.addHeader( CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED );
        request.setTextBody( "grant_type=client_credentials" );

        return getToken( client, amServer, clientAuth, request, authEntryName );
    }

    public static OAuthToken refreshToken( HttpClient client, String amServer, String clientAuth,
            OAuthToken currentToken )
    {
        HttpRequest request = HttpClientFactory.defaultRequest( "POST", false );

        request.addHeader( CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED );
        request.setTextBody( "grant_type=refresh_token&refresh_token=" + currentToken.getRefreshToken() );

        return getToken( client, amServer, clientAuth, request, currentToken.getAuthEntryName() );
    }

    public static OAuthToken getTokenForClientUser( HttpClient client, String amServer, String clientAuth,
            String userAuth, String scope, String tokenEntryName, boolean formOnly )
    {
        HttpAuth clientCred = client.httpClientConfig().getAuths().get( clientAuth );
        HttpAuth userCred   = client.httpClientConfig().getAuths().get( userAuth );

        if ( userCred != null )
        {
            HttpRequest request = HttpClientFactory.defaultRequest( "POST", false );
            request.addHeader( CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED );

            StringBuilder sb = new StringBuilder();
            sb.append( "grant_type=password&" );
            if ( formOnly )
            {
                sb.append( combineAuth( clientCred, "client_id", "client_secret" ) );
                sb.append( "&" );
            }
            String s = scope != null && !scope.isEmpty() ? "&scope=" + StringHelper.encodeUrl( scope ) : "";
            sb.append( combineAuth( userCred, "username", "password" ) );
            sb.append( s );
            request.setTextBody( sb.toString() );

            return getToken( client, amServer, formOnly ? null : clientAuth, request, tokenEntryName );

        }
        else
        {
            LOGGER.error( "missing user authorization key '{}'", userAuth );
        }

        return null;
    }

    private static String combineAuth( HttpAuth a, String lk, String pk )
    {
        if ( a == null )
        {
            return "";
        }
        return lk + "=" + StringHelper.encodeUrl( a.getAuthName() ) + "&" + pk + "=" + StringHelper.encodeUrl(
                a.getAuthPassword() );
    }

    public static OAuthToken getToken( HttpClient client, String authServer, String clientAuthKey, HttpRequest request,
            String authEntryName )
    {
        if ( client.httpClientConfig().getAddresses().containsKey( "oauth2-access-token" ) )
        {
            ResponseData rd = client.execute( authServer, clientAuthKey, "oauth2-access-token", request );
            rd.showResponse( false, Level.DEBUG );

            if ( rd.getStatus() == 200 )
            {
                OAuthToken token = extractOAuthToken( rd.getResponseContent() );
                if ( token != null )
                {
                    token.setUsedHostKey( authServer );
                    token.setUsedClientKey( clientAuthKey );
                    token.setAuthEntryName( authEntryName );
                    client.putAuth( authEntryName, EnumAuthType.BEARER, token.getAccessToken(), "", false );
                    return token;
                }
            }
        }
        else
        {
            LOGGER.error( "missing 'oauth2-access-token' address, can't connect Server to request access token" );
        }

        return null;
    }

    private static <T> T readJsonPath( ReadContext ctx, String path, Class<T> tClass )
    {
        try
        {
            return ctx.read( path, tClass );
        }
        catch ( PathNotFoundException ex )
        {
            return null;
        }
    }

    public static OAuthToken extractOAuthToken( String content )
    {
        if ( null == content || content.isEmpty() )
        {
            return null;
        }

        OAuthToken token = new OAuthToken();

        // extract the bearer token from the response data
        Configuration config = Configuration.defaultConfiguration().addOptions( Option.SUPPRESS_EXCEPTIONS );
        ReadContext   ctx    = JsonPath.using( config ).parse( content );

        token.setAccessToken( StringHelper.nonNullStr( readJsonPath( ctx, "$.access_token", String.class ) ) );
        token.setRefreshToken( StringHelper.nonNullStr( readJsonPath( ctx, "$.refresh_token", String.class ) ) );
        token.setTokenType( StringHelper.nonNullStr( readJsonPath( ctx, "$.token_type", String.class ) ) );
        token.setScope( StringHelper.nonNullStr( readJsonPath( ctx, "$.scope", String.class ) ) );
        Long t = readJsonPath( ctx, "$.expires_in", Long.class );
        if ( null != t )
        {
            token.setExpiryTimeMs( System.currentTimeMillis() + ( 1000 * t ) );
        }

        if ( token.isValid() )
        {
            LOGGER.info( "    access-token : {}", token.getAccessToken() );
            LOGGER.info( "   refresh-token : {}", token.getRefreshToken() );
            LOGGER.info( "            type : {}", token.getTokenType() );
            if ( token.getExpiryTimeMs() > 0L )
            {
                Date date = new Date( token.getExpiryTimeMs() );
                SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss z" ); // the format of your date
                String formattedDate = sdf.format( date );
                LOGGER.info( "         expires : {}", formattedDate );
            }
            else
            {
                LOGGER.info( "         expires : <not received>" );
            }

            if ( !"bearer".equalsIgnoreCase( token.getTokenType() ) )
            {
                LOGGER.error( "token type '{}' not supported (bearer only) ", token.getTokenType() );
                token = null;
            }
        }

        return token;
    }

    /**
     * @param content       the json response from the request
     * @param isRpcResponse true if the json has the Account Manager RPC format, otherwise normal OAuth format
     * @return returns the access token from the content or null if malformed
     * @deprecated please replace with extractOAuthToken(String content, boolean isRpcResponse), the result from the
     * deprecated method is OAuthToken.getAccessToken(), result is null too if no response is found.
     */
    @Deprecated
    public static String extractBearerToken( String content, boolean isRpcResponse )
    {

        String token;
        String tokenType;
        Long   expTime;

        if ( null == content || content.isEmpty() )
        {
            return null;
        }

        // extract the bearer token from the response data
        try
        {
            Configuration config = Configuration.defaultConfiguration().addOptions( Option.SUPPRESS_EXCEPTIONS );
            ReadContext   ctx    = JsonPath.using( config ).parse( content );

            if ( isRpcResponse )
            {
                token = ctx.read( "$.token_creation_result.access_token" );
                tokenType = ctx.read( "$.token_creation_result.token_type" );
                expTime = ctx.read( "$.token_creation_result.expiry_time", Long.class );
            }
            else
            {
                token = ctx.read( "$.access_token" );
                tokenType = ctx.read( "$.token_type" );
                expTime = ctx.read( "$.expires_in", Long.class );
                if ( null != expTime )
                {
                    expTime = System.currentTimeMillis() + ( 1000 * expTime );
                }
            }
        }
        catch ( PathNotFoundException ex )
        {
            LOGGER.error( ex );
            token = null;
            tokenType = "";
            expTime = null;
        }

        if ( null != token )
        {
            tokenType = tokenType.trim();

            LOGGER.info( "   token is : {}", token );
            LOGGER.info( "       type : {}", tokenType );
            if ( expTime != null )
            {
                Date date = new Date( expTime );
                SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss z" ); // the format of your date
                String formattedDate = sdf.format( date );
                LOGGER.info( "    expires : {}", formattedDate );
            }
            else
            {
                LOGGER.info( "    expires : <not received>" );
            }

            if ( !"bearer".equalsIgnoreCase( tokenType ) )
            {
                LOGGER.error( "token type '{}' not supported (bearer only) ", tokenType );
                token = null;
            }
        }

        return token;
    }
}
