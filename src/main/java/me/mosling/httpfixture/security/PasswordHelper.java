package me.mosling.httpfixture.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class PasswordHelper
{

    private static final Logger LOGGER = LogManager.getLogger( PasswordHelper.class );

    private PasswordHelper()
    {
        // functional class
    }

    public static boolean validatePbkdf2Password( String pbdkfHash, String password )
    {
        String[] parts      = pbdkfHash.split( ":" );
        int      iterations = Integer.parseInt( parts[0] );
        byte[]   salt       = fromHex( parts[1] );
        byte[]   hash       = fromHex( parts[2] );

        try
        {
            PBEKeySpec       spec     = new PBEKeySpec( password.toCharArray(), salt, iterations, hash.length * 8 );
            SecretKeyFactory skf      = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            byte[]           testHash = skf.generateSecret( spec ).getEncoded();

            int diff = hash.length ^ testHash.length;
            for ( int i = 0; i < hash.length && i < testHash.length; i++ )
            {
                diff |= hash[i] ^ testHash[i];
            }
            return diff == 0;
        }
        catch ( NoSuchAlgorithmException | InvalidKeySpecException ex )
        {
            LOGGER.fatal( "Can't create PBKDF Hash: {}", ex.getMessage() );
            return false;
        }
    }

    public static String generatePbkdf2PasswordHash( String password )
    {
        int    iterations = 1000;
        char[] chars      = password.toCharArray();

        try
        {
            byte[]           salt = getSalt();
            PBEKeySpec       spec = new PBEKeySpec( chars, salt, iterations, 64 * 8 );
            SecretKeyFactory skf  = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            byte[]           hash = skf.generateSecret( spec ).getEncoded();

            return String.format( "%d:%s:%s", iterations, toHex( salt ), toHex( hash ) );
        }
        catch ( NoSuchAlgorithmException | InvalidKeySpecException ex )
        {
            LOGGER.fatal( "Can't create PBKDF Hash: {}", ex.getMessage() );
            return "no-hash-created";
        }
    }

    private static byte[] getSalt()
            throws NoSuchAlgorithmException
    {
        SecureRandom sr   = SecureRandom.getInstance( "SHA1PRNG" );
        byte[]       salt = new byte[16];
        sr.nextBytes( salt );
        return salt;
    }

    private static String toHex( byte[] array )
    {
        BigInteger bi            = new BigInteger( 1, array );
        String     hex           = bi.toString( 16 );
        int        paddingLength = ( array.length * 2 ) - hex.length();
        if ( paddingLength > 0 )
        {
            return String.format( String.format( "%%0%dd", paddingLength ) , 0 ) + hex;
        }
        else
        {
            return hex;
        }
    }

    private static byte[] fromHex( String hex )
    {
        byte[] bytes = new byte[hex.length() / 2];
        for ( int i = 0; i < bytes.length; i++ )
        {
            bytes[i] = (byte) Integer.parseInt( hex.substring( 2 * i, 2 * i + 2 ), 16 );
        }
        return bytes;
    }

}
