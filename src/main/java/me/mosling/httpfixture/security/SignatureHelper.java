package me.mosling.httpfixture.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

public class SignatureHelper
{

    private static final Logger LOGGER = LogManager.getLogger( SignatureHelper.class );

    private SignatureHelper()
    {
    }

    public static byte[] signMessage( final String message, final PrivateKey privateKey,
            final String signatureAlgorithm )
    {
        byte[] signature = {};
        try
        {
            byte[]    data    = message.getBytes( StandardCharsets.UTF_8 );
            Signature signAlg = Signature.getInstance( signatureAlgorithm );
            signAlg.initSign( privateKey );
            signAlg.update( data );
            signature = signAlg.sign();
        }
        catch ( SignatureException | InvalidKeyException | NoSuchAlgorithmException e )
        {
            LOGGER.error( "signMessage: {}", e );
        }

        return signature;
    }

    public static boolean verifyMessage( final String message, final byte[] signature, final PublicKey publicKey,
            final String signatureAlgorithm )
    {
        boolean isVerified = false;
        try
        {
            Signature signAlg = Signature.getInstance( signatureAlgorithm );
            signAlg.initVerify( publicKey );
            signAlg.update( message.getBytes( StandardCharsets.UTF_8 ) );
            isVerified = signAlg.verify( signature );
        }
        catch ( SignatureException | InvalidKeyException | NoSuchAlgorithmException  e )
        {
            LOGGER.error( "length of signature {} : ", signature.length );
            LOGGER.error( "verifySignature with {}: {}", signatureAlgorithm, e.getMessage() );
        }

        return isVerified;
    }
}
