package me.mosling.httpfixture.common;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonHelper
{
    private static final Logger LOGGER = LogManager.getLogger( CommonHelper.class );

    private CommonHelper()
    {
        // private default constructor, helper class offers only static methods
    }

    public static InputStream getInputStreamFromName( String name, boolean allowSystemAccess )
    {
        return getInputStreamFromName( name, allowSystemAccess, false );
    }

    /**
     * The method use ClassLoader().getResourceAsStream() to read a resource, that means NO leading slash
     * is needed to address the resources.
     *
     * @param name              first try open a resource, if not found try open a file if allowSystemAccess is true
     * @param allowSystemAccess also check the filesystem if there exists a file with this name and use it
     * @return InputStream or null if neither resource or file found
     */
    public static InputStream getInputStreamFromName( String name, boolean allowSystemAccess, boolean silent )
    {
        if ( null == name || name.isEmpty() )
        {
            return null;
        }

        String resName = name.trim();
        if ( resName.startsWith( "/" ) )
        {
            resName = "." + resName;
        }
        InputStream stream = CommonHelper.class.getClassLoader().getResourceAsStream( resName );
        if ( stream != null )
        {
            return stream;
        }

        if ( allowSystemAccess )
        {
            try
            {
                return new FileInputStream( new File( name ) );
            }
            catch ( FileNotFoundException ex )
            {
                if ( !silent )
                {
                    LOGGER.error( "can't found resource or file for name '{}'", name );
                }
            }
        }
        else
        {
            LOGGER.error( "can't found resource '{}'", name );
        }

        return null;
    }

    public static byte[] getHeaderFromName( String name, boolean allowSystemAccess, int countBytes )
    {
        try
        {
            InputStream is = getInputStreamFromName( name, allowSystemAccess );
            if ( null != is )
            {
                int    cb = Math.min( countBytes, is.available() );
                byte[] b  = new byte[cb];
                int    l  = is.read( b, 0, cb );
                if ( l != cb )
                {
                    LOGGER.warn( "read {} bytes only from {} (expected {} bytes)", l, name, cb );
                }
                is.close();
                return b;
            }
        }
        catch ( IOException e )
        {
            LOGGER.error( e );
        }

        return new byte[0];
    }

    /**
     * @param path the path that should exist or will be created
     * @return true if the path exists
     */
    public static boolean createFolderStructure( String path )
    {
        boolean bOk = true;
        if ( null != path && !path.isEmpty() )
        {
            File folder = new File( path );
            bOk = folder.isDirectory();
            if ( !bOk )
            {
                bOk = folder.mkdirs();
            }
            if ( !bOk )
            {
                LOGGER.error( "can't create folder '" + folder.getAbsolutePath() + "'" );
            }
        }
        return bOk;
    }

    public static File openExistingFile( String fileName )
    {
        if ( fileName == null || fileName.isEmpty() )
        {
            return null;
        }

        File nf = new File( fileName );
        if ( nf.exists() )
        {
            return nf;
        }
        else
        {
            LOGGER.error( "File '{}' doesn't exists", nf.getAbsoluteFile() );
        }

        return null;
    }

    public static void logListElements( Level ll, String name, List<Object> objectList )
    {
        String t  = "                 ";
        String s  = "              |  ";
        int    nl = name.length();
        int    tl = t.length();
        if ( nl >= tl )
        {
            t = name.substring( 0, tl - 2 ) + "..";
        }
        else
        {
            t = t.substring( 0, tl - nl ) + name;
        }

        int idx     = 0;
        int lastIdx = objectList.size() - 1;
        for ( Object o : objectList
                .stream()
                .sorted( Comparator.comparing( Object::toString ) )
                .collect( Collectors.toList() ) )
        {
            LOGGER.log( ll, "{} : {}", ( 0 == idx || lastIdx == idx ) ? t : s, o.toString() );
            idx++;
        }
    }


}
