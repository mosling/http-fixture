package me.mosling.httpfixture.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;

public class StringHelper
{
    private static final Logger LOGGER = LogManager.getLogger( StringHelper.class );

    public static String nonNullStr( String s )
    {
        return s == null ? "" : s;
    }

    public static String byteCountDisplay( long bytes )
    {
        if ( 0 == bytes )
        {
            return "zero bytes";
        }

        long[]   l = { 1024L, 1024L, 1024L, 1024L, 1024L, 1024L, Long.MAX_VALUE };
        String[] u = { "byte", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB" };
        String   s = "";
        long     v = bytes;

        for ( int i = 0; i < l.length; ++i )
        {
            long r = v % l[i];
            if ( r != 0 )
            {
                s = String.format( "%d%s %s", Math.abs( r ), u[i], s );
            }
            v = v / l[i];
        }

        return ( bytes < 0 ? "-" : "" ) + s.trim();
    }

    public static String millisecondsToTimeString( long ms, boolean withZero )
    {
        long[]   l = { 1000L, 60L, 60L, 24L, Long.MAX_VALUE };
        String[] u = { "ms", "s", "m", "h", "days" };
        String   s = "";
        long     v = ms;

        for ( int i = 0; i < l.length; ++i )
        {
            long r = v % l[i];
            if ( r != 0 || ( withZero && v > 0 ) )
            {
                s = String.format( "%d%s %s", Math.abs( r ), u[i], s );
            }
            v = v / l[i];
        }

        return ( ms < 0 ? "-" : "" ) + s.trim();
    }

    public static Base64.Decoder examineDecoder( String str )
    {
        boolean isUrlEncode = str.contains( "-" ) || str.contains( "_" );
        return isUrlEncode ? Base64.getUrlDecoder() : Base64.getDecoder();
    }

    public static String base64Encoding( String txt )
    {
        return Base64.getEncoder().encodeToString( txt.getBytes( StandardCharsets.UTF_8 ) );
    }

    public static String encodeUrl( String s )
    {
        try
        {
            return URLEncoder
                    .encode( s, StandardCharsets.UTF_8.name() )
                    .replaceAll( "\\+", "%20" )
                    .replaceAll( "\\*", "%2A" );
        }
        catch ( UnsupportedEncodingException e )
        {
            LOGGER.error( "can't encode '{}' using {}", s, StandardCharsets.UTF_8.name() );
            LOGGER.error( e );
        }

        return s;
    }

    public static String hexify( byte[] bytes )
    {

        char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        StringBuilder buf = new StringBuilder( bytes.length * 3 );

        for ( int i = 0; i < bytes.length; ++i )
        {
            if ( i > 0 )
            {
                buf.append( ':' );
            }
            buf.append( hexDigits[( bytes[i] & 0xf0 ) >> 4] );
            buf.append( hexDigits[bytes[i] & 0x0f] );
        }

        return buf.toString();
    }

    public static String getUtcString( Date d )
    {

        return DateTimeFormatter.ISO_INSTANT.format( d.toInstant() );
    }

    public static String findProperty( String propertyName, String defaultValue )
    {
        String rv = System.getenv( propertyName );
        if ( null == rv )
        {
            rv = System.getProperty( propertyName, defaultValue );
        }

        return rv;
    }
}
