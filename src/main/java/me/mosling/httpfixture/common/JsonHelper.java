package me.mosling.httpfixture.common;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Map;

public class JsonHelper
{
    private static final Logger LOGGER = LogManager.getLogger( JsonHelper.class );

    private JsonHelper()
    {
    }

    private static void writeJsonField( JsonGenerator jg, String key, String value )
            throws IOException
    {
        String  strValue  = value;
        boolean boolValue = true;
        boolean isNumber  = false;
        boolean isBoolean = false;
        boolean isNull    = false;

        // remove quote sign if the string starts with '#'
        if ( value.startsWith( "##" ) )
        {
            strValue = value.substring( 1 );
        }
        else if ( value.startsWith( "#" ) )
        {
            if ( "#false".equalsIgnoreCase( value ) )
            {
                boolValue = false;
                isBoolean = true;
            }
            else if ( "#true".equalsIgnoreCase( value ) )
            {
                isBoolean = true;
            }
            else if ( "#null".equalsIgnoreCase( value ) )
            {
                isNull = true;
            }
            else
            {
                isNumber = true;
            }
        }

        if ( isBoolean )
        {
            writeBoolean( jg, key, boolValue );
        }
        else if ( isNumber )
        {
            writeNumber( jg, key, value );
        }
        else if ( isNull )
        {
            writeNull( jg, key );
        }
        else
        {
            writeString( jg, key, strValue );
        }
    }

    private static void writeNull( JsonGenerator jg, String key )
            throws IOException
    {
        if ( null == key )
        {
            jg.writeNull();
        }
        else
        {
            jg.writeNullField( key );
        }
    }

    private static void writeString( JsonGenerator jg, String key, String strValue )
            throws IOException
    {
        if ( null == key )
        {
            jg.writeString( strValue );
        }
        else
        {
            jg.writeStringField( key, strValue );
        }
    }

    private static void writeNumber( JsonGenerator jg, String key, String value )
            throws IOException
    {
        long intValue;
        intValue = 0;
        if ( value.length() > 1 )
        {
            intValue = Long.parseLong( value.substring( 1 ) );
        }
        if ( null == key )
        {
            jg.writeNumber( intValue );
        }
        else
        {
            jg.writeNumberField( key, intValue );
        }
    }

    private static void writeBoolean( JsonGenerator jg, String key, boolean boolValue )
            throws IOException
    {
        if ( null == key )
        {
            jg.writeBoolean( boolValue );
        }
        else
        {
            jg.writeBooleanField( key, boolValue );
        }
    }

    /**
     * @param jsonList a simple list representing the JSON elements
     * @return the resulting json document or empty string usage "_v,1,get_tenant_information_request,{,arg0,pod8,
     * arg1,[,*_prd,*_s01";
     * A non string entry value (number, boolean) starts with an '#' sign, you can quote this by typing "##".
     */
    public static String createJsonFromList( final List<String> jsonList )
    {
        if ( null == jsonList )
        {
            return "";
        }

        JsonFactory           jf = new JsonFactory();
        ByteArrayOutputStream b  = new ByteArrayOutputStream();
        String                retVal;

        try
        {
            JsonGenerator    jg         = jf.createGenerator( b );
            Deque<Character> blockStack = new ArrayDeque<>();
            String           varName    = "";

            jg.writeStartObject();
            blockStack.push( 'O' );

            for ( String s : jsonList )
            {
                if ( "[".equalsIgnoreCase( s ) || "]".equalsIgnoreCase( s ) )
                {
                    varName = computeSquareBracket( jg, blockStack, s, varName );
                }
                else if ( "{".equalsIgnoreCase( s ) || "}".equalsIgnoreCase( s ) )
                {
                    varName = computeBrace( jg, blockStack, s, varName );
                }
                else
                {
                    varName = computeField( jg, blockStack, s, varName );
                }
            }
            closeOpenTags( jg, blockStack );
            jg.close();

            retVal = b.toString( StandardCharsets.UTF_8.name() );
        }
        catch ( IOException e )
        {
            LOGGER.error( e );
            retVal = "";
        }

        return retVal;
    }

    public static String createJsonFromMap( Map<String, String> m )
    {
        List<String> l = new ArrayList<>();
        for ( Map.Entry<String, String> e : m.entrySet() )
        {
            l.add( e.getKey() );
            l.add( e.getValue() );
        }

        return createJsonFromList( l );
    }

    private static void closeOpenTags( JsonGenerator jg, Deque<Character> blockStack )
            throws IOException
    {
        while ( !blockStack.isEmpty() )
        {
            Character c = blockStack.pop();
            if ( c == 'A' )
            {
                jg.writeEndArray();
            }
            else
            {
                jg.writeEndObject();
            }
        }
    }

    private static String computeSquareBracket( JsonGenerator jg, Deque<Character> blockStack, String s,
            String varNameArg )
            throws IOException
    {
        String  varName = varNameArg;
        boolean inArray = peekIs( blockStack, 'A' );
        if ( "]".equalsIgnoreCase( s ) )
        {
            if ( inArray )
            {
                jg.writeEndArray();
                blockStack.pop();
            }
            else
            {
                throw new IOException( "Closing array without open it." );
            }
        }
        else if ( "[".equalsIgnoreCase( s ) )
        {
            if ( inArray )
            {
                jg.writeStartArray();
            }
            else if ( !varName.isEmpty() )
            {
                jg.writeArrayFieldStart( varName );
            }
            else
            {
                throw new IOException( "Open unnamed array." );
            }
            varName = "";
            blockStack.push( 'A' );
        }

        return varName;
    }

    private static boolean peekIs( Deque<Character> blockState, Character c )
    {
        Character pv = blockState.peek();

        return pv != null && pv.equals( c );
    }

    private static String computeBrace( JsonGenerator jg, Deque<Character> blockStack, String s, String varNameArg )
            throws IOException
    {
        boolean inArray = peekIs( blockStack, 'A' );
        String  varName = varNameArg;
        if ( "{".equalsIgnoreCase( s ) )
        {
            if ( inArray )
            {
                jg.writeStartObject();
            }
            else if ( !varName.isEmpty() )
            {
                jg.writeObjectFieldStart( varName );
            }
            else
            {
                throw new IOException( "Open unnamed object." );
            }
            varName = "";
            blockStack.push( 'O' );
        }
        else if ( "}".equalsIgnoreCase( s ) )
        {
            if ( peekIs( blockStack, 'O' ) )
            {
                jg.writeEndObject();
                blockStack.pop();
            }
            else
            {
                throw new IOException( "Closing object without open it." );
            }
        }

        return varName;
    }

    private static String computeField( JsonGenerator jg, Deque<Character> blockStack, String s, String varNameArg )
            throws IOException
    {
        String varName = varNameArg;
        if ( peekIs( blockStack, 'A' ) )
        {
            writeJsonField( jg, null, s );
        }
        else
        {
            if ( !varName.isEmpty() )
            {
                writeJsonField( jg, varName, s );
                varName = "";
            }
            else
            {
                varName = s;
            }
        }
        return varName;
    }

    public static String createJsonFromString( final String jsonString, final String separator )
    {
        if ( null == jsonString || null == separator )
        {
            return "";
        }
        return createJsonFromList( Arrays.asList( jsonString.split( separator, -1 ) ) );
    }

}
