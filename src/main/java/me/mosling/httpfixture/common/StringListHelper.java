package me.mosling.httpfixture.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class StringListHelper
{
    public static final Comparator<String> integerStringComparator = ( String s1, String s2 ) -> {
        try
        {
            int i1 = Integer.parseInt( s1 );
            int i2 = Integer.parseInt( s2 );
            return Integer.compare( i1, i2 );
        }
        catch ( NumberFormatException | IndexOutOfBoundsException e )
        {
            // do nothing
        }
        return s1.compareTo( s2 );
    };
    /**
     * @param msg              A list of comma separated strings. Every string is
     *                         trimmed before the compressed list is created
     * @param maxStartSequence maximal length of start sequence
     * @return compressed list by collect strings with the same start sequence to an array like format
     */
    public static String compressStringList( String msg, int maxStartSequence )
    {
        if ( null == msg || msg.isEmpty() )
        {
            return "";
        }

        List<String> l = new ArrayList<>( Arrays.asList( msg.split( "," ) ) );
        l.replaceAll( String::trim );
        l.sort( integerStringComparator );

        return compressArrayList( l, maxStartSequence );
    }

    /**
     * @param l                A sorted list of strings.
     * @param maxStartSequence maximal length of start sequence
     * @return compressed list by collect strings with the same start sequence to an array like format
     */
    public static String compressArrayList( List<String> l, int maxStartSequence )
    {
        if ( null == l || l.isEmpty() )
        {
            return "";
        }

        Map<String, List<String>> m  = new TreeMap<>();
        int                       sl = 0;
        int                       nextSl;
        String                    startSeq;
        int                       sz = l.size();

        for ( int i = 0; i < sz; ++i )
        {
            nextSl = i + 1 < sz ? findEqualStartSequence( l.get( i ), l.get( i + 1 ), maxStartSequence ) : 0;
            sl = nextSl > sl ? nextSl : sl;

            startSeq = sl > 0 ? l.get( i ).substring( 0, sl ) : l.get( i );

            if ( !m.containsKey( startSeq ) )
            {
                m.put( startSeq, new ArrayList<>() );
            }

            if ( sl > 0 )
            {
                m.get( startSeq ).add( l.get( i ).substring( sl ) );
            }
        }

        StringBuilder sb = new StringBuilder();
        for ( Map.Entry<String, List<String>> e : m.entrySet() )
        {
            if ( sb.length() > 0 )
            {
                sb.append( ", " );
            }
            sb.append( e.getKey() );
            if ( e.getValue().size() > 1 )
            {
                sb.append( "[" );
                sb.append(
                        e.getValue().stream().sorted( integerStringComparator ).collect( Collectors.joining( "," ) ) );
                sb.append( "]" );
            }
            else if ( !e.getValue().isEmpty() )
            {
                sb.append( e.getValue().get( 0 ) );
            }
        }

        return sb.toString();
    }

    private static int findEqualStartSequence( String s1, String s2, int mc )
    {
        if ( null != s1 && !s1.isEmpty() && null != s2 && !s2.isEmpty() )
        {
            int m = Math.min( mc, Math.min( s1.length(), s2.length() ) );
            int l = 0;
            for ( int i = 0; i < m; ++i )
            {
                if ( s1.charAt( i ) == s2.charAt( i ) )
                {
                    l++;
                }
                else
                {
                    return l;
                }
            }

            return l;
        }

        return 0;
    }
}
