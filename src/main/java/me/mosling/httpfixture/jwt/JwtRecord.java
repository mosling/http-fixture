package me.mosling.httpfixture.jwt;

import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.security.SignatureHelper;
import lombok.Getter;
import lombok.Setter;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class JwtRecord
{
    @Getter private static final String JWT_SIGNATURE_ALGORITHM = "SHA256withRSA";

    // @formatter:off
    @Getter @Setter String header = "";
    @Getter @Setter String payload = "";
    @Getter @Setter String signature = "";
    // @formatter:on

    public JwtRecord importString( String jwt )
    {
        List<String> parts = Arrays.asList( jwt.split( "\\." ) );

        header = !parts.isEmpty() ?
                new String( StringHelper.examineDecoder( parts.get( 0 ) ).decode( parts.get( 0 ) ) ) :
                "";
        payload = parts.size() > 1 ?
                new String( StringHelper.examineDecoder( parts.get( 1 ) ).decode( parts.get( 1 ) ) ) :
                "";
        signature = parts.size() > 2 ?
                new String( StringHelper.examineDecoder( parts.get( 2 ) ).decode( parts.get( 2 ) ) ) :
                "";

        return this;
    }

    public String buildAndSign( PrivateKey signatureKey )
    {
        String header64  = Base64.getEncoder().encodeToString( header.getBytes( StandardCharsets.UTF_8 ) );
        String payload64 = Base64.getEncoder().encodeToString( payload.getBytes( StandardCharsets.UTF_8 ) );
        String jwtData64 = header64 + "." + payload64;

        byte[] jwtSignature = SignatureHelper.signMessage( jwtData64, signatureKey, JWT_SIGNATURE_ALGORITHM );
        signature = Base64.getEncoder().encodeToString( jwtSignature );

        // create json web token
        return jwtData64 + "." + signature;
    }
}
