package me.mosling.httpfixture.jwt;

import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.security.SecurityHelper;
import me.mosling.httpfixture.security.SignatureHelper;
import lombok.Getter;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class JwtHelper
{
    @Getter private static final String JWT_SIGNATURE_ALGORITHM = "SHA256withRSA";

    private JwtHelper()
    {
    }

    public static String getJwtNameForSignAlgorithm( String algName )
    {
        String jwtName;
        switch ( algName )
        {
            case JWT_SIGNATURE_ALGORITHM:
                jwtName = "RS256";
                break;
            case "SHA384withRSA":
                jwtName = "RS384";
                break;
            case "SHA512withRSA":
                jwtName = "RS512";
                break;
            default:
                jwtName = "";
                break;
        }

        return jwtName;
    }

    public static String explainJwt( String jwt )
    {
        List<String>  pn    = Arrays.asList( "Header             : ", "Payload            : ",
                "Signature (Base64) : " );
        List<String>  parts = Arrays.asList( jwt.split( "\\." ) );
        StringBuilder sb    = new StringBuilder( "\n" );

        for ( int i = 0; i < pn.size(); ++i )
        {
            sb.append( pn.get( i ) );
            if ( parts.size() > i )
            {
                if ( i > 1 )
                {
                    sb.append( parts.get( i ) );
                }
                else
                {
                    Base64.Decoder decoder = StringHelper.examineDecoder( parts.get( i ) );
                    sb.append( new String( decoder.decode( parts.get( i ) ), StandardCharsets.UTF_8 ) );
                }
            }
            sb.append( "\n" );
        }
        return sb.toString();
    }

    public static boolean verifyJwt( String jwt, String certificate )
    {
        X509Certificate cert = SecurityHelper.createX509Certificate( certificate );
        return verifyJwt( jwt, cert != null ? cert.getPublicKey() : null );
    }

    public static boolean verifyJwt( String jwt, PublicKey publicKey )
    {
        List<String> parts = Arrays.asList( jwt.split( "\\." ) );

        if ( parts.size() > 2 )
        {
            if ( null == publicKey )
            {
                return false;
            }

            Base64.Decoder decoder = StringHelper.examineDecoder( parts.get( 2 ) );

            return SignatureHelper.verifyMessage( parts.get( 0 ) + "." + parts.get( 1 ),
                    decoder.decode( parts.get( 2 ) ), publicKey, JWT_SIGNATURE_ALGORITHM );
        }

        return true;
    }

}
