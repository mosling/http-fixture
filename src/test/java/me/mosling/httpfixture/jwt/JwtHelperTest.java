package me.mosling.httpfixture.jwt;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Base64;

import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import me.mosling.httpfixture.security.SecurityHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class JwtHelperTest
{
    private static final String testKeyAlias = "self-signed-saml-sp";

    private static JksManager jksManager;

    private final static String extMsg = "eyJraWQiOiJyc2ExIiwiYWxnIjoiUlMyNTYifQ"
            + ".eyJzdWIiOiJiZTczMmJiNy0yNGUyLTQ2OTQtOGQ1OC00MjkzZDg0NGUzOWUiLCJpc3MiOiJiZTczMmJiNy0yNGUyL"
            + "TQ2OTQtOGQ1OC00MjkzZDg0NGUzOWUiLCJleHAiOjE1MTMwNjY3NzV9.Fgr9ClBaR1kXYxnteNO1b3zeC9SEpl83Ew"
            + "zdwZhxC_AvB-DW18F_4eTENSwszvvgxYMk837ySs2J_vUsDXQo9L7QWGoze5j9UcoCFAom_Z1kufjIEtaUwpWVeHt4"
            + "RhGgbt0cDrB8qwBqm_TO0sCwjBVdbrOKy3TVYHlbH_E-Zm3VRV7APDAEd_YuIYmlTp6_axCnexov_vejYSuRE-T2MM"
            + "LOAwjsF08qeV9K7oJL_57xI3ZaN5BqbMwmcFDFudaWyYydi1yItbIAv1IDUJ7jf_eYzuU5SIbdcMiNEkpVaAwRtk7w"
            + "gyP4cFIGI8eLhuihrTPPDsm7hgZpyI-2dhmz2g";

    private final static String extCertificate = "MIIDETCCAfmgAwIBAgIEU8SXLjANBgkqhkiG9w0BAQsFADA5MRswGQYDVQQKExJv"
            + "cGVuYW0uZXhhbXBsZS5jb20xGjAYBgNVBAMTEWp3dC1iZWFyZXItY2xpZW50MB4X"
            + "DTE0MTAyNzExNTY1NloXDTI0MTAyNDExNTY1NlowOTEbMBkGA1UEChMSb3BlbmFt"
            + "LmV4YW1wbGUuY29tMRowGAYDVQQDExFqd3QtYmVhcmVyLWNsaWVudDCCASIwDQYJ"
            + "KoZIhvcNAQEBBQADggEPADCCAQoCggEBAID4ZZ/DIGEBr4QC2uz0GYFOCUlAPanx"
            + "X21aYHSvELsWyMa7DJlD+mnjaF8cPRRMkhYZFXDJo/AVcjyblyT3ntqL+2Js3D7T"
            + "mS6BSjkxZWsJHyhJIYEoUwwloc0kizgSm15MwBMcbnksQVN5VWiOe4y4JMbi30t6"
            + "k38lM62KKtaSPP6jvnW1LTmL9uiqLWz54AM6hU3NlCI3J6Rfh8waBIPAEjmHZNqu"
            + "Ol2uGgWumzubYDFJbomLSQqO58RuKVaSVMwDbmENtMYWXIKQL2xTt5XAbwEQEgJ/"
            + "zskwpA2aQt1HE6de3UymOhONhRiu4rk3AIEnEVbxrvy4Ik+wXg7LZVsCAwEAAaMh"
            + "MB8wHQYDVR0OBBYEFIuI7ejuZTg5tJsh1XyRopGOMBcsMA0GCSqGSIb3DQEBCwUA"
            + "A4IBAQBM/+/tYYVIS6LvPl3mfE8V7x+VPXqj/uK6UecAbfmRTrPk1ph+jjI6nmLX"
            + "9ncomYALWL/JFiSXcVsZt3/412fOqjakFVS0PmK1vEPxDlav1drnVA33icy1wORR"
            + "Ru5/qA6mwDYPAZSbm5cDVvCR7Lt6VqJ+D0V8GABFxUw9IaX6ajTqkWhldY77usvN"
            + "eTD0Xc4R7OqSBrnASNCaUlJogWyzhbFlmE9Ne28j4RVpbz/EZn0oc/cHTJ6Lryzs"
            + "ivf4uDO1m3M3kM/MUyXc1Zv3rqBjTeGSgcqEAd6XlGXY1+M/yIeouUTi0F1bk1rN" + "lqJvd57Xb4CEq17tVbGBm0hkECM8";

    private final static String b64Jwt =
            "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKU1V6STFOaUo5LmV5SnpkV0lpT2lKcWQzUWdjM1ZpYW1WamRDSXNJbVY0"
                    + "Y0NJNk5qQjkua21Hd25GRURSYXhZZlh5bk50bjkyNjRUVVNiUUlSUENIcU1XRnAzem5EWk9EYUNJK1dRSD"
                    + "dDOTF0UHJ1L1VjRW5LSjdJMWdVUXgzeWJrUzlqbEpmM3JjeW8vaWVJNSs0ZERLTUJ1eDRwSDMxcjR1c3dD"
                    + "ZC9Gc0VOaXdVaEE0Tjh0MXJHSm85dzhWNkpReTdSRWc5bkt6czJwV0dxMmRIOUJJNkFTdW9oZVRkaUlnek"
                    + "U0RGMrZ3JjK29iMGpiVDgvU01mcm5jS0VRaGNBT2hDRmNYRE16NDh1dUl3TWh3ckFhQURpRVk5cjZTZ0dX"
                    + "QUVtaHYvdEc2S1RkTkQ3UjRkQzc2K0tDWW5PK1R4LzBIdC9vd0lHdnZFVGJRcFI1ekljQjNPa051QzVSR1"
                    + "lJQkxwNHd4WmJpWDNEZTVZT3VHaEhJRldQbGJNYjFVd1JzR05iM3pqcllRPT0=";

    @BeforeAll
    public static void setUp()
    {
        jksManager = new JksManager( new JksManagerArgs()
                .keystore( "certs/self-signed-saml-sp.jks" )
                .keystoreSecret( "4demandware" )
                .keystoreKeySecret( "qatest" ) );
    }

    @Test
    public void getJwtNameForSignAlgorithm()
    {

        assertThat( JwtHelper.getJwtNameForSignAlgorithm( "unknown" ) ).isEmpty();
        assertThat( JwtHelper.getJwtNameForSignAlgorithm( "SHA256withRSA" ).equals( "RS256" ) );
    }

    @Test
    public void verifySelfJwt()
    {
        assertThat( JwtHelper.verifyJwt( b64Jwt, jksManager.getPublicKey( testKeyAlias ) ) ).isTrue();
    }

    @Test
    public void explainJwt()
    {
        String jwtString = new String( Base64.getDecoder().decode( b64Jwt ), StandardCharsets.UTF_8 );
        String explained = JwtHelper.explainJwt( jwtString );

        assertThat( explained ).contains( "Header             : {\"typ\":\"JWT\",\"alg\":\"RS256\"}" );
        assertThat( explained ).contains( "Payload            : {\"sub\":\"jwt subject\",\"exp\":60}" );
    }

    @Test
    public void verifyExternalJwt()
    {
        X509Certificate cert = SecurityHelper.createX509Certificate( extCertificate );
        assertThat( JwtHelper.verifyJwt( extMsg, cert != null ? cert.getPublicKey() : null ) ).isTrue();

    }

    @Test
    public void verifyExternalJwt2()
    {
        assertThat( JwtHelper.verifyJwt( extMsg, extCertificate ) ).isTrue();
    }
}