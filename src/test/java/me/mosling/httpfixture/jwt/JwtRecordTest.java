package me.mosling.httpfixture.jwt;

import me.mosling.httpfixture.common.JsonHelper;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

public class JwtRecordTest
{
    private static       JksManager jksManager;
    private static final String     testKeyAlias = "self-signed-saml-sp";

    @BeforeAll
    public static void setUp()
    {
        jksManager = new JksManager( new JksManagerArgs()
                .keystore( "certs/self-signed-saml-sp.jks" )
                .keystoreSecret( "4demandware" )
                .keystoreKeySecret( "qatest" ) );
    }

    @Test
    public void importString()
    {
        // create header and payload
        String header = JsonHelper.createJsonFromString( "typ,JWT,alg,RS256", "," );
        String payload = JsonHelper.createJsonFromString(
                MessageFormat.format( "sub,{0},exp,#{1}", "jwt subject", 60000 / 1000 ), "," );

        JwtRecord jr = new JwtRecord().importString(
                StringHelper.base64Encoding( header ) + "." + StringHelper.base64Encoding( payload ) );

        assertThat( jr.getHeader() ).isEqualTo( header );
        assertThat( jr.getPayload() ).isEqualTo( payload );
        assertThat( jr.getSignature() ).isEmpty();
    }

    @Test
    public void buildAndSign()
    {
        // create header and payload
        String header = JsonHelper.createJsonFromString( "typ,JWT,alg,RS256", "," );
        String payload = JsonHelper.createJsonFromString(
                MessageFormat.format( "sub,{0},exp,#{1}", "jwt subject", 60000 / 1000 ), "," );

        JwtRecord jr = new JwtRecord();
        jr.setHeader( header );
        jr.setPayload( payload );
        String jwt   = jr.buildAndSign( jksManager.getPrivateKey( testKeyAlias, "qatest" ) );
        String jwt64 = Base64.getEncoder().encodeToString( jwt.getBytes( StandardCharsets.UTF_8 ) );

        assertThat( jwt64 ).isEqualTo( b64Jwt );
    }

    private final static String b64Jwt =
            "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKU1V6STFOaUo5LmV5SnpkV0lpT2lKcWQzUWdjM1ZpYW1WamRDSXNJbVY0"
                    + "Y0NJNk5qQjkua21Hd25GRURSYXhZZlh5bk50bjkyNjRUVVNiUUlSUENIcU1XRnAzem5EWk9EYUNJK1dRSD"
                    + "dDOTF0UHJ1L1VjRW5LSjdJMWdVUXgzeWJrUzlqbEpmM3JjeW8vaWVJNSs0ZERLTUJ1eDRwSDMxcjR1c3dD"
                    + "ZC9Gc0VOaXdVaEE0Tjh0MXJHSm85dzhWNkpReTdSRWc5bkt6czJwV0dxMmRIOUJJNkFTdW9oZVRkaUlnek"
                    + "U0RGMrZ3JjK29iMGpiVDgvU01mcm5jS0VRaGNBT2hDRmNYRE16NDh1dUl3TWh3ckFhQURpRVk5cjZTZ0dX"
                    + "QUVtaHYvdEc2S1RkTkQ3UjRkQzc2K0tDWW5PK1R4LzBIdC9vd0lHdnZFVGJRcFI1ekljQjNPa051QzVSR1"
                    + "lJQkxwNHd4WmJpWDNEZTVZT3VHaEhJRldQbGJNYjFVd1JzR05iM3pqcllRPT0=";

}