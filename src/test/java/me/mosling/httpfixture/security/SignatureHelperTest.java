package me.mosling.httpfixture.security;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SignatureHelperTest
{

    private static final String testKeyAlias = "self-signed-saml-sp";

    private static JksManager jksManager;

    @BeforeAll
    public static void setUp()
    {
        jksManager = new JksManager( new JksManagerArgs()
                .keystore( "certs/self-signed-saml-sp.jks" )
                .keystoreSecret( "4demandware" )
                .keystoreKeySecret( "qatest" ) );
    }

    @Test
    public void signAndVerify()
    {
        String message = "This is the message to sign.";

        String an = jksManager.getSignatureAlgorithmName( testKeyAlias );

        byte[] msgSign = SignatureHelper.signMessage( message, jksManager.getPrivateKey( testKeyAlias, "qatest" ), an );

        boolean b = SignatureHelper.verifyMessage( message, msgSign, jksManager.getPublicKey( testKeyAlias ), an );

        assertThat( b ).isTrue();
    }
}
