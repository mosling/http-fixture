package me.mosling.httpfixture.security;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class PasswordHelperTest
{

    @Test
    public void validatePassword()
    {
        String h = PasswordHelper.generatePbkdf2PasswordHash( "password" );
        assertThat( PasswordHelper.validatePbkdf2Password( h, "password" ) ).isTrue();
        assertThat( PasswordHelper.validatePbkdf2Password( h, "passwOrd" ) ).isFalse();
    }

    @Test
    public void toHex()
    {
        byte[] src = { (byte) 254, 2, 54, 4 };
        assertThat( executeToHex( Arrays.copyOf( src, 2 ) ) ).isEqualTo( "fe02" );
        assertThat( executeToHex( Arrays.copyOf( src, 3 ) ) ).isEqualTo( "fe0236" );
        assertThat( executeToHex( Arrays.copyOfRange( src, 2, 3 ) ) ).isEqualTo( "36" );
        assertThat( executeToHex( Arrays.copyOfRange( src, 2, 4 ) ) ).isEqualTo( "3604" );
        assertThat( executeToHex( Arrays.copyOfRange( src, 1, 4 ) ) ).isEqualTo( "023604" );
    }

    private String executeToHex( byte... b )
    {
        try
        {
            Method method = PasswordHelper.class.getDeclaredMethod( "toHex", byte[].class );
            method.setAccessible( true );
            Object ro = method.invoke( null, (Object) b );
            if ( ro instanceof String )
            {
                return ro.toString();
            }
        }
        catch ( NoSuchMethodException e )
        {
            System.err.println( "ERROR: toHex(byte[]) not exists " + e.getMessage() );
        }
        catch ( IllegalAccessException | InvocationTargetException e )
        {
            System.err.println( "ERROR executing toHex(byte[]) " + e.getMessage() );
        }

        return "";
    }
}