package me.mosling.httpfixture.security;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JksManagerTest
{
    private final static String certificate = "-----BEGIN CERTIFICATE-----\n"
            + "MIID4TCCAsmgAwIBAgIJAPgUfEzow97/MA0GCSqGSIb3DQEBBQUAMIGGMQswCQYD\n"
            + "VQQGEwJVUzEYMBYGA1UECgwPRGVtYW5kd2FyZSBJbmMuMRYwFAYDVQQIDA1NYXNz\n"
            + "YWNodXNldHRzMRMwEQYDVQQHDApCdXJsaW5ndG9uMRQwEgYDVQQLDAtFbmdpbmVl\n"
            + "cmluZzEaMBgGA1UEAwwRY2EuZGVtYW5kd2FyZS5jb20wHhcNMTMwOTIyMDg0MDE0\n"
            + "WhcNMjMwOTIwMDg0MDE0WjCBhjELMAkGA1UEBhMCVVMxGDAWBgNVBAoMD0RlbWFu\n"
            + "ZHdhcmUgSW5jLjEWMBQGA1UECAwNTWFzc2FjaHVzZXR0czETMBEGA1UEBwwKQnVy\n"
            + "bGluZ3RvbjEUMBIGA1UECwwLRW5naW5lZXJpbmcxGjAYBgNVBAMMEWNhLmRlbWFu\n"
            + "ZHdhcmUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsXWPSvle\n"
            + "AHFP+jhR61W07p8ZpAn/7PnjuaQ/sOdTSknb8khpSaeKGx49kA/oQfzohVRELs7y\n"
            + "sccFT3bVxapMT98ffCzNzDJG4BzaOWkFwGNs0TG9HxB3J4AT4zXeNBdEciwv0VAB\n"
            + "nAqQN2mStgzk8GYKpm2xZvmNzpqohvEE6tb76CAxonPpFRn4A5tz8kB75UO90klf\n"
            + "oaasZNNR35dFGgvCzQtGnyzLf+IAqoq9ujCvmLrWeyXF0dDMEZoho4CQ8IQb5qB/\n"
            + "RITAwLggjjgLzLGtZpP4MouT1O90zw48RJxiDngZmygOLzeeeQ1i1PvHhjSH5Hbr\n"
            + "5wrgHVw2mWzj4QIDAQABo1AwTjAdBgNVHQ4EFgQUU8Z2tVYPl6a0ZTA9zBtHRd4m\n"
            + "fWEwHwYDVR0jBBgwFoAUU8Z2tVYPl6a0ZTA9zBtHRd4mfWEwDAYDVR0TBAUwAwEB\n"
            + "/zANBgkqhkiG9w0BAQUFAAOCAQEADoNf+DHNHESPYRMt10jae48pUbHEVYdXhH1f\n"
            + "paouqZbHgFIOnkJfH6Qr/reDvjobOj+pjSrKCCm0WwcLQvd6UVzjIYUvctPx7LZ7\n"
            + "2cCbOSuLenwNUzpP7qNfwC0Zd2DkQ/9MnrZldACr3SGUjWUkn8tu+ICO15vw6N94\n"
            + "nrW34I1qsbhIH51u6g6RdiR33V5z0HcyMIvQdxYfLqzpofOQ7zLjM8LYq7gaiJi6\n"
            + "RNAePr/Nq++QfLO4hKfqSMCZ0vGPzJTXlcHC1rGJev7PIXV0JsnIeCoY5BqpAAvw\n"
            + "OR0BH1SNaQfvz6cQYxpbxvTnf/ExSLxL8cByeWQ53pPRGHn1JQ==\n" + "-----END CERTIFICATE-----";

    private final static String privKey = "-----BEGIN PRIVATE KEY-----\n"
            + "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCxdY9K+V4AcU/6\n"
            + "OFHrVbTunxmkCf/s+eO5pD+w51NKSdvySGlJp4obHj2QD+hB/OiFVEQuzvKxxwVP\n"
            + "dtXFqkxP3x98LM3MMkbgHNo5aQXAY2zRMb0fEHcngBPjNd40F0RyLC/RUAGcCpA3\n"
            + "aZK2DOTwZgqmbbFm+Y3OmqiG8QTq1vvoIDGic+kVGfgDm3PyQHvlQ73SSV+hpqxk\n"
            + "01Hfl0UaC8LNC0afLMt/4gCqir26MK+YutZ7JcXR0MwRmiGjgJDwhBvmoH9EhMDA\n"
            + "uCCOOAvMsa1mk/gyi5PU73TPDjxEnGIOeBmbKA4vN555DWLU+8eGNIfkduvnCuAd\n"
            + "XDaZbOPhAgMBAAECggEBAJmrMmV7ISKC/P2xrYZ7Vk1YKoz5N8xO7BsCl0CqYF6x\n"
            + "LKxWdg4Y7afj/O12SOpzhiqGuPBLtq+ppviT8DPV73UuwoX2ClPrqaJCedpDDn71\n"
            + "2fIg/yMaUAQ9jXswV3WGPHgduV6oyGwGXbIOUsQg0FM4GbravEdujJS/KnaH2Xk9\n"
            + "NbGGzZEobnMLALBheh1ST0x8/LuO7oyTaYTwOwCbOdtQJNWLUGFqKZlyS26qQ+t6\n"
            + "553l3cCtZbhfR9Ahw+ch/p3NKyufI5Nudf19d2qnwFuWYWDLYvVTZXU/RmDLw4Rs\n"
            + "Jq/sA08kK2x5KlSPT178skSv1bKHSnFyIv3eAfCznwECgYEA3q8UUgKSj1dLmbbo\n"
            + "vSnwSOGqLKe3TBWRXhp+k3NwBZRm2JGuCP0pf2xKb82X0E6/2wxQk0TetYtE9/Vg\n"
            + "JqrwV6p91Rby1VxjWq4gN7o3KECe+eqJFdUR/mVwcANIxbw6fSsyGVCmM6jzwRAL\n"
            + "F6CM9hH4Dl9Q/a5QfxuIr19itnECgYEAzAJZXKd9f5A/h+XP3a05JB6vzhIJgwmC\n"
            + "iaOd+Ff2GptNGr5u6RFPBtORoCH9edGzLBDmJwQn6xV+iwNi/TV2IQz9Pu+kNl/p\n"
            + "48iW52ppKLY3ItYOBBTGbtdyhCAzrDiV4dHs/YeXltk6P5r94eQmyCxfQJOzU24a\n"
            + "ckHF9dBGHHECgYBIawWfughxp0EFx08fHYFwIjtWdGCO1MDGqrUZPMRkfCOKMK45\n"
            + "bCNPtibq/yBs9P89vF6D1jTbM2SFgv329UZeLCrp3qGwM3B4MAJ4PQ3tAD7zJYvZ\n"
            + "VsaYReLv+pFzBMi8pgyP4QN772j+EzqYP3U4HRXfjq3nIgY5n/mulsWicQKBgDA0\n"
            + "xeTs9uyzqy9zkGCzU/NDBQvOm0zJbH0VmnhaYJ7834VYRmDwZBoa2Chcn8avVIUv\n"
            + "IRNLGhukopg6nqNyhI1ewmCiw8zNcTkP5wibzml+VRz4M6unNRRv+HFcIpmpbVFF\n"
            + "B5kI9zr/7ZGmECPWg4t9YKyjuhYWyE0DWcGyCz8hAoGAZulJ/YychHbyaCPvERO1\n"
            + "v+MI1UhxPegugKGLrcIjLFG6TxFtjg051B9GSTYeA6TdJIAWcZVq9l4UG1/ANXF5\n"
            + "G/FmONpK35CWo0zuV1zGtpbp7Ns8rrGM+8ZNd+aYhvS40wcaFkW6MLEzYjl405OQ\n" + "ZfX2KO3zUZMQqRcFk7+nQ4s=\n"
            + "-----END PRIVATE KEY-----";

    private final static String p12 = "MIIKkAIBAzCCCkoGCSqGSIb3DQEHAaCCCjsEggo3MIIKMzCCBXAGCSqGSIb3DQEHAaCCBWEEggVd\n"
            + "MIIFWTCCBVUGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBQOIRkGYDWYWl0O\n"
            + "eJSKSFv9DxTzvgICBAAEggTIdIg4JbxnNxKw5XS+5lJdFCalPAfM3TVs0IaeY5LSc7eRNuzCj7lV\n"
            + "odxRCi3gSd9Eb91p5oWbEcQTTrZjoP0GmqsJszFbTeCTMJn+AqVajIoBX+faxM0zj4RuyEVHrRmw\n"
            + "4w/r2qihC3mHS6DJa3j8c48sV1M6jKoHhacrE67c4T5s/diYuY5DgMkbgPyy8y+X6V45dcXQFVyo\n"
            + "1KYaVZqfUXOMDUXYvqHDN+qrjVHHuweZX63uXwGmkLMIQjNYoYSEwkfUXlE1nYII9SB0RDdeNSen\n"
            + "BDUg3MfwWNdU6fsgdcEoOvUPw3h8wUmNdS6N5G++xdTO2qjvCHgnP5LOrRL7c5gxqvnpGxfQTqEc\n"
            + "FAS8BLsTU2c0Q0Qj9gGNABwYqWaG9Ux9QTJN2j/tzJf0RnRW0JSHxwHXRtpApQQ1gUhEchr8auEn\n"
            + "0Yvk4VQt99rYg9iL9zW+qeYIXOPSgruc5HLB3AkHM/1+O5nGuhAzvY+bNykehL/w4y8R9JUfb4xj\n"
            + "qHRK5pZ3jtVRJfSfB3/dcDwZ1bPZd2zLcRkAYUmTQUiWoMUyhb9uhrjFzamP5ogLB73tucIoW/pt\n"
            + "OyhZ/rEdj1Jubx3nfQUCL8xOBdLxZc/W+WtNb7Ln8xkAdtrv2l970eLBTFNqQg8zTogGvlKHBaIk\n"
            + "uAhauSvYRgBEzHL+/VMVzyMHIMvQyrtNTevnCLD63eYtYLL2vnBuRSPJbrWSF+WX/hX8VEYmW8SG\n"
            + "by6jry7PiXKIqkxFoKJehXTHEf1dYM3JTrL1KHwvOqIfc8PRd9xNrqQukB8Z0MSFTudU78tXvQWe\n"
            + "Bz8g4+Gv2dbCSsjHyXyfiZPZp4l8PEZAaMwxSTQq4jZYxh8/5oMpE6t2A72QbbgH0CsvbxZOL9vs\n"
            + "FqmEdUf1brvxh03qBK4KMVvwSlY++pvwxpbJFf8T9DLD3Cack8IJGte2NL88gGz17S2M7rA7r2AN\n"
            + "3j7VrRLDiXwA+KSeYBiVTE3Whh8odUKkrEmVK3UdlBIzuZGIDpyjKRYNCLi+yDFyshnxefHKgBbZ\n"
            + "51wvwROhKYdQQA5Ek3FwYtglUSiVOb/U0sOuDWDaY8qfYf0SZ2130/otuF/7W3fCvl3wyF9994uX\n"
            + "JVGyE3ui5G3jRmp5gGiIqhQqthLQIapzDcIzTkk33ATythPiBfQ0XxAYj/mYMIMeF+BCdfkRDcjy\n"
            + "P5HWECAlJSuSBa7n8bZMvpsaDcE2ujlaAJPZEN4XH9MEqUEdlfFiCwGjTagRopr2yxuBMd94JDU/\n"
            + "xRDt35rbki8hQRLNCBywRoJSanYkIxiLIOIBBzsAvW7ToOrXxdYeB9KpOU5mw0KTwC53QBGtskvB\n"
            + "ZG4+0J+OmreDSSHd8rP6EU/ofFXXPBSNkEkm9DdKXJI1KDoUK3QQazG4VL2vqU+sPOaiZYZul3bG\n"
            + "frU4Bzr/JRtCbjaiLXeYJOYaZWa9uZpsmEHni0jn7/gsY4PYWzPttYD1QOUlhSwcTOJhWk/c0ALS\n"
            + "evNahK6ASqvjiruQ9rHL/2zG33UoDBBhctTKgVYMKFS4OTVFpPWJUXz/tlzRYIQteSPk6L6xNVVI\n"
            + "B2kpfsMWos7PbSJ2FOccXrfXbMOdKrNBJBQb+bGM6j+sBhaZtpmRj30CMx1/MUgwIQYJKoZIhvcN\n"
            + "AQkUMRQeEgBhAHUAdABoAHoAXwByAHAAYzAjBgkqhkiG9w0BCRUxFgQUU8Z2tVYPl6a0ZTA9zBtH\n"
            + "Rd4mfWEwggS7BgkqhkiG9w0BBwagggSsMIIEqAIBADCCBKEGCSqGSIb3DQEHATAoBgoqhkiG9w0B\n"
            + "DAEGMBoEFEnLeSBgRK9zFy6oFJ8/3aY5ePBAAgIEAICCBGj+AtxHrlWJmSRzdaU97K3N4dozVlsD\n"
            + "AtAjMWs/rTcine9WEIKHJhWsWAFXMrLvvEq6VlVZQIgImkxH2ui3q3SfHuWCC4cqwzNkGRWHoCOZ\n"
            + "3GjiUV6u3/dNEDQSlTEWsCYtOVBYh3sCpWJGtaBzH6KD8fuaoO2kjSyCZpH1LkcSshLXE+rRpr2w\n"
            + "fioR1wC1Q5u+wVdb+WNmpcWQf5VXi6WvO+xnTXsEyQIjQtb7VlwTMAuENTIrTtQe7PgUPtld4jtf\n"
            + "2f7MaTxLbRmNWnxMkR79EA5YFEr1vW+eUdPSQ+0W/esC1o8g6oK8xcFlOVzkFv4xvqvclXltRvnd\n"
            + "VIT3lbiEhZLg7DvnxwbkuDrO6RRJnYUrnY/MQn9ExRKNKuqQ+8j1TxDQ6TAU2XA5RSxbXvIJUpq1\n"
            + "pSvx1TkXAIZogiM0nf3j7PxRqtC/NMm0p02kn1lQYJY20DcITznMgkeWN0kO42CUBL8bWUGtHdGx\n"
            + "1AVNTYwi8iEvDgkG2v5w0x5DAQmGykRIE333rvXWk4hm/EirMUQkr3/q9uI1EGuMmq6LDgGhxvZR\n"
            + "EOP5G2tKiX9Cj0181RdDmeJaEniremYY0Gunj1IwkHFKJzEBOAvXYuCj7uSI3ZLc8XlqJqaB/2Jt\n"
            + "D7aC0T1SW809IgAuoqhHhf99oG7z+sEUbH0MGr8QVwbariU8H+SPsYilTc0M/t4LE+/8P677FgOk\n"
            + "rfYQz7dqtoxUJhLIKL0MyTj5kwKl2ShWWjjj9uAHU4Ujk6BkJLarjozLLYgUGQK5gqFDwDo8wCgM\n"
            + "SoCZL+wuj69Ha5AK2e4Hm4xxXz1cqJrLiM7rwV30OrGaZ3RZc+eiawfI9wGX2vVcJ412nsocrsZU\n"
            + "xS9sOtk7n8f2zJacMazX55Zp7jKc+ENkZkBcdEeSkd1Xqhhy408wR+3Dkgxh6y6xyobpcE3CdrEa\n"
            + "8IOFuesCUL5z6RaGe0ayuOsEM5ACitJZwXfV6AWa/EsapbB3/MKlF5SXjOiWnUuMI/SzC82uX64d\n"
            + "LQoeqT6RegpJJPFnqknCxiPP8nctLKRWs+S9zlJrEpAcSDWunke74XB8AsAt4cxSD4n3ttMbntCD\n"
            + "90/pBDflhIqmVm4f8tXMk7IJfm7d/xoymUJ3KzsrY5BZvhMYhe8Pzj00iaMPDPZSNzc0eJn+YfNT\n"
            + "RddqFZ2TrHSDmxjcN4Dp2IQLyR0ZpkkTelFum+zVcnly7rq7QugxUFwveutI6vdo2Kv4IG8T9Ltv\n"
            + "X4FP9IoqQ5XhiqaMQW+rUoS57JbQ+qN/Ke0jFAcjLtTImTbOzEShWkpcXgNZrjjk/mUf0F3MvTuF\n"
            + "jErAb4yLTyaXeF8JVSOP0j0v0bq5yE9myjW9dJPGDgW4LKFQGxp9jfkIiV4BYhGNyyP/r/XKkRot\n"
            + "DHcrshu3539qsR2aRTjeBA/PtWdUl80OKVNzDSEhUOeZ2bsC7XejhZc5s8s2TZvwuyrCnzuu3pSI\n"
            + "5FhU34ZDjAOm7AofT6Ap+62jl8UBKLIwPTAhMAkGBSsOAwIaBQAEFDJyHE4NZBVgWzIs1mCxNeG7\n"
            + "1LAsBBRFjQyKxqkHVxPmt9fGbYqA5DTOtAICBAA=\n";

    @Test
    public void putPrivateKeyFromString()
    {
        JksManager m = new JksManager( new JksManagerArgs() );
        m.putPrivateKeyFromString( "test_auth", privKey, certificate );

        assertThat( m.getPrivateKey( "test_auth", m.args().keystoreKeySecret() ) ).isNotNull();
    }

    @Test
    public void putP12FromString()
    {
        JksManager m = new JksManager( new JksManagerArgs() );
        m.importKeystoreEntry( p12, "mystore", "authz_rpc", "fromVault", true );

        assertThat( m.getPrivateKey( "fromVault", m.args().keystoreKeySecret() ) ).isNotNull();
    }

    //    @Test
    //    void checkVaultKeystore()
    //    {
    //        BaseTestFixture f = new BaseTestFixture();
    //
    //        f.getClient().putHost( "vault", "http", "localhost", 8200 );
    //        String vt = VaultClient.getAccountManagerReaderToken( f, "vault",
    // "a8ffd88e-5123-766e-e0fb-41f573c87244" );
    //        String cc = VaultClient.readValue( f, "vault", vt, "v1/secret/accountmanager/auth_rpc_b64" );
    //
    //        String ks = JsonPath.read( cc, "$.data.value" );
    //        String kp = JsonPath.read( cc, "$.data.storepwd" );
    //
    //        assertThat( ks, is( p12 ) );
    //        assertThat( kp, is( "mystore" ) );
    //
    //        JksManager m = new JksManager().keystorePassword( "secret" ).build();
    //        m.importKeystoreEntry( ks, kp, "authz_rpc", "authz_rpc", "localSecured", true );
    //        System.out.println( m.toString() );
    //
    //        assertThat( m.getPrivateKey( "authz_rpc", "localSecured" ), not( is( nullValue() ) ) );
    //    }
}
