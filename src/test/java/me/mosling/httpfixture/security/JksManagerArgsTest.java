package me.mosling.httpfixture.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class JksManagerArgsTest
{
    @Test
    public void useJsonConfig()
            throws JsonProcessingException
    {
        final ObjectMapper mapper = new ObjectMapper();
        JksManagerArgs jma = new JksManagerArgs().configFile( "./test-jks-manager.json" );

        assertThat( jma.truststore() ).isEqualTo( "TestTruststore.jks" );
        assertThat( jma.truststoreSecret() ).isEqualTo( "HiddenTrustSecret" );

        assertThat( jma.keystore() ).isEqualTo( "TestKeystore.jks" );
        assertThat( jma.keystoreSecret() ).isEqualTo( "HiddenSecret" );
        assertThat( jma.keystoreKeySecret() ).isEqualTo( "HiddenKeySecret" );

        assertThat( jma.jdkIncludeCertificates() ).isEqualTo( true );
        assertThat( jma.jdkAliasPostfix() ).isEqualTo( "-jdk" );
        assertThat( jma.jdkKeystoreFilename() ).isEqualTo( JksManagerArgs.JDK_SYSTEM_KEYSTORE );

        assertThat( jma.allowFilesystemLookup() ).isEqualTo( true );
        assertThat( jma.debugMode() ).isEqualTo( true );

        String json = mapper.writeValueAsString( jma );

        ReadContext ctx = JsonPath.parse( json );
        assertThat( ctx.read( "$.truststore", String.class ) ).isEqualTo( "TestTruststore.jks" );
        assertThat( ctx.read( "$.keystore", String.class ) ).isEqualTo( "TestKeystore.jks" );

        assertThatThrownBy( () -> {
            ctx.read( "$.keystoreSecret", String.class );
        } ).isInstanceOf( PathNotFoundException.class );

        assertThatThrownBy( () -> {
            ctx.read( "$.truststoreSecret", String.class );
        } ).isInstanceOf( PathNotFoundException.class );

        assertThatThrownBy( () -> {
            ctx.read( "$.keystoreKeySecret", String.class );
        } ).isInstanceOf( PathNotFoundException.class );

        assertThat( ctx.read( "$.allowFilesystemLookup", Boolean.class ) ).isTrue();
        assertThat( ctx.read( "$.debugMode", Boolean.class ) ).isTrue();
        assertThat( ctx.read( "$.jdkIncludeCertificates", Boolean.class ) ).isTrue();
    }

}