package me.mosling.httpfixture.security;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SecurityHelperTest
{
    private static JksManager jksManager;

    @BeforeAll
    public static void setUp()
    {
        jksManager = new JksManager(
                new JksManagerArgs().truststore( "certs/truststore.jks" ).truststoreSecret( "4demandware" ) );
    }

    @Test
    public void certificateInformation()
    {
        List<String> cl = SecurityHelper.certificateInformation( jksManager.getCertificate( "dw.root.self" ), false );

        assertThat( cl ).contains( "  version          : V1 " );
        assertThat( cl ).contains( "  valid from .. to : 2013-09-22T08:38:58Z .. 2023-09-20T08:38:58Z " );
        assertThat( cl ).contains(
                "  SHA-1            : 51:61:2b:27:e7:93:fe:a9:1b:af:bd:2b:3c:03:7a:52:3f:03:df:2f " );
    }

}