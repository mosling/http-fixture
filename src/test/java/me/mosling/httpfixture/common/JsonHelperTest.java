package me.mosling.httpfixture.common;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.jupiter.api.Test;

public class JsonHelperTest
{

    @Test
    public void to_few_arguments()
    {
        String j = JsonHelper.createJsonFromString( "234", "," );
        assertThat( j ).isEqualTo( "{}" );
    }

    @Test
    public void missing_object_name()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,{,arg0,67", "," );
        assertThat( j ).isEqualTo( "" );
    }

    @Test
    public void missing_array_name()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,[,arg0,67", "," );
        assertThat( j ).isEqualTo( "" );
    }

    @Test
    public void missing_open_bracket()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,],67,]", "," );
        assertThat( j ).isEqualTo( "" );
    }

    @Test
    public void nested_brackets()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,[,#67,#78,[,#56,#78,],],}", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":[67,78,[56,78]]}}" );
    }

    @Test
    public void empty_array()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,[,],}", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":[]}}" );
    }

    @Test
    public void string_array()
    {
        String j = JsonHelper.createJsonFromString( "domain,[,abc,def,ghi,]", "," );
        assertThat( j ).isEqualTo( "{\"domain\":[\"abc\",\"def\",\"ghi\"]}" );
    }

    @Test
    public void default_number_value()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,#", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":0}}" );
    }

    @Test
    public void default_string_value()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,,", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":\"\"}}" );
    }

    @Test
    public void boolean_values()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,#true,arg1,#false", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":true,\"arg1\":false}}" );
    }

    @Test
    public void create_missing_endArray()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,[,#67,78", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":[67,\"78\"]}}" );
    }

    @Test
    public void quote_hashsign()
    {
        String j = JsonHelper.createJsonFromString( "_v,1,operation,{,arg0,##hashsign", "," );
        assertThat( j ).isEqualTo( "{\"_v\":\"1\",\"operation\":{\"arg0\":\"#hashsign\"}}" );
    }

    @Test
    public void using_separator()
    {
        String j = JsonHelper.createJsonFromString( "_vö5öanother,operationö{öarg0önameöarg1övalue", "ö" );
        assertThat( j ).isEqualTo( "{\"_v\":\"5\",\"another,operation\":{\"arg0\":\"name\",\"arg1\":\"value\"}}" );
    }

    @Test
    public void null_values()
    {
        String j = JsonHelper.createJsonFromString( "name,#null", "," );
        assertThat( j ).isEqualTo( "{\"name\":null}" );
    }

    @Test
    public void callWithNullpointer()
    {
        assertThat( JsonHelper.createJsonFromList( null ) ).isEqualTo( "" );
        assertThat( JsonHelper.createJsonFromString( null, "+" ) ).isEqualTo( "" );
        assertThat( JsonHelper.createJsonFromString( "Text", null ) ).isEqualTo( "" );
    }

    @Test
    public void mapToJson()
    {
        Map<String, String> m = new TreeMap<>();
        m.put( "string", "str" );
        m.put( "int", "#42" );
        m.put( "nptr", "#null" );
        m.put( "bool", "#true" );

        String j = JsonHelper.createJsonFromMap( m );
        assertThat( j ).isEqualTo( "{\"bool\":true,\"int\":42,\"nptr\":null,\"string\":\"str\"}" );

    }

    @Test
    public void booleanArray()
    {
        assertThat( JsonHelper.createJsonFromString( "flags,[,#true,#false,#false", "," ) ).isEqualTo(
                "{\"flags\":[true,false,false]}" );
    }

    @Test
    public void closingWithoutOpening()
    {
        assertThat( JsonHelper.createJsonFromString( "flags,}", "," ) ).isEqualTo( "{}" );
    }

    @Test
    public void complexStructureTest()
    {
        String body = JsonHelper.createJsonFromString( MessageFormat.format(
                "authId,{0},template,,state,DataStorage1,header,Welcome to Commerce Cloud,callbacks,[,'{',type,"
                        + "NameCallback,output,[,'{',name,prompt,value, User "
                        + "Name: ,'}',],input,[,'{',name,IDToken1,value,{1},'}',],'}','{',type,PasswordCallback,output,"
                        + "[,'{'," + "name,prompt,value, Password: ,'}',],input,[,'{',name,IDToken2,value,{2}",
                "tokenid", "username", "secret" ), "," );

        assertThat( body.length() ).isEqualTo( 359 );
    }

    @Test
    public void nestedArrayObjects()
    {
        List<String> l = new ArrayList<>();
        l.add( "parser_errors" );
        l.add( "[" );
        l.add( "{" );
        l.add( "line" );
        l.add( "id.eq.'1234" );
        l.add( "errors" );
        l.add( "[" );
        l.add( "{" );
        l.add( "error_msg" );
        l.add( "line 1:6 token recognition error at: ''1234'" );
        l.add( "}" );
        l.add( "{" );
        l.add( "error_msg" );
        l.add( "line 1:11 mismatched input '<EOF>' expecting {Q" );
        l.add( "pos" );
        l.add( "id.eq.'1234--><--" );
        l.add( "}" );
        l.add( "]" );
        l.add( "}" );
        l.add( "]" );
        l.add( "}" );

        String j = JsonHelper.createJsonFromList( l );
        System.out.println(j);
    }
}
